import smbus

class SX1509:
    REG_PULLUP_B = 0x06
    REG_PULLUP_A = 0x07
    REG_PULLDN_B = 0x08
    REG_PULLDN_A = 0x09
    REG_OPEN_DRAIN_B = 0x0A
    REG_OPEN_DRAIN_A = 0x0B
    REG_POLARITY_B = 0x0C
    REG_POLARITY_A = 0x0D
    REG_DIRECTION_B = 0x0E
    REG_DIRECTION_A = 0x0F
    REG_DATA_B = 0x10
    REG_DATA_A = 0x11

    def __init__(self, busnum, addr):
        self.bus = smbus.SMBus(busnum)
        self.addr = addr

    def setRegBits(self, reg, mask, val):
        # get current value
        tmp = self.bus.read_byte_data(self.addr, reg)
        # print('reg', hex(reg), 'mask', mask, 'val', val, 'tmp', tmp)
        # clear all mask bits
        tmp &= ~mask
        # set mask bits if val
        if val:
            tmp |= mask
        tmp = self.bus.write_byte_data(self.addr, reg, tmp)
        # print('reg', hex(reg), 'mask', mask, 'val', val, 'tmp', tmp)

    def setPin(self, pin, value=False, pullup=False, pulldown=False, opendrain=False, polarity=False, direction=False):
        if pin < 8:
            bank = 0
            mask = 1 << pin
        else:
            bank = -1
            mask = 1 << (pin-8)

        self.setRegBits(SX1509.REG_PULLUP_A + bank, mask, pullup)
        self.setRegBits(SX1509.REG_PULLDN_A + bank, mask, pulldown)
        self.setRegBits(SX1509.REG_OPEN_DRAIN_A + bank, mask, opendrain)
        self.setRegBits(SX1509.REG_POLARITY_A + bank, mask, polarity)
        self.setRegBits(SX1509.REG_DIRECTION_A + bank, mask, direction)
        self.setRegBits(SX1509.REG_DATA_A + bank, mask, value)


