#!/usr/bin/env python3

from threading import Thread
import time
from datetime import datetime, timedelta
import cherrypy
from SX1509 import SX1509
import os
import pickle

saved_state_file = 'sprinkler.state'

# smbus 1, addr 0x3e
io = SX1509(1, 0x3e)

num_outputs = 8

class Cycle(object):
    def __init__(self):
        self.on_minutes = 0
        self.time_start = [0,0]
        self.days = [False for i in range(7)]

if os.path.isfile(saved_state_file):
    with open(saved_state_file, 'rb') as f:
        states = pickle.load(f)
else:
    states = [Cycle() for i in range(num_outputs)]

class Sprinkler(object):
    @cherrypy.expose
    def index(self, **args):
        if args.keys():
            for s in states:
                s.days = [False for i in range(7)]
            for k,v in args.items():
                spl = k.split('_')
                if spl[1] == 'hour':
                    states[int(spl[0])].time_start[0] = int(v)
                if spl[1] == 'minute':
                    states[int(spl[0])].time_start[1] = int(v)
                if spl[1] == 'duration':
                    states[int(spl[0])].on_minutes = int(v)
                if spl[1] == 'day':
                    states[int(spl[0])].days[int(spl[2])] = True
            with open(saved_state_file, 'wb') as f:
                pickle.dump(states, f)
        ret = '<html><body>'
        ret += '<form method="post" action="/">'

        for i in range(num_outputs):
            ii = str(i)
            ret += ii+': duration(minutes) <input type="text" value="'+str(states[i].on_minutes)+'" name="'+ii+'_duration" />'
            ret += 'hour <input type="text" value="'+str(states[i].time_start[0])+'" name="'+ii+'_hour" />'
            ret += 'minute <input type="text" value="'+str(states[i].time_start[1])+'" name="'+ii+'_minute" />'
            for d,dd in enumerate('MTWRFSS'):
                checked = 'checked' if states[i].days[d] else ''
                if checked:
                    print('checked', i, d)
                ret += dd+'<input type="checkbox" name="'+ii+'_day_'+str(d)+'" ' + checked + ' />'
            ret += '</br>'
        ret += '<button type="submit">Update</button>'
        ret += '</form></body></html>'
        return ret

cherrypy.config.update({'server.socket_host': '0.0.0.0',
                        'server.socket_port': 80})

run_sched = True
def runScheduler():
    while run_sched:
        now = datetime.today()
        for i,s in enumerate(states):
            d = datetime(now.year, now.month, now.day, s.time_start[0], s.time_start[1])
            d2 = d+timedelta(minutes=s.on_minutes)
            if now > d and now < d2 and now.weekday() in [i for i,j in enumerate(s.days) if j]:
                # false == open drain low relay on
                io.setPin(i, False, opendrain=True)
                # io.setPin(i, True, pullup=True, pulldown=True)
            else:
                io.setPin(i, True, opendrain=True)
                # io.setPin(i, False, pullup=True, pulldown=True)
        time.sleep(1)

def killSched():
    global run_sched
    run_sched = False

killSched.priority = 10
cherrypy.engine.subscribe('stop', killSched)
sched = Thread(target=runScheduler)
sched.start()

webapp = Sprinkler()
cherrypy.quickstart(Sprinkler())
run_sched = False

print('disabling all outputs')
for i in range(num_outputs):
    io.setPin(i, True, opendrain=True)
